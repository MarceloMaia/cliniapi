from distutils.core import setup

setup(
    name='cliniapi',
    version='1',
    packages=['cliniapi', 'cliniapi/endpoints', 'cliniapi/models', 'cliniapi/serializers', 'cliniapi/tests'],
    url='https://bitbucket.org/MarceloMaia/cliniapi',
    license='',
    author='marcelo',
    author_email='mmaia.cc@gmail.com',
    description='teste'
)
