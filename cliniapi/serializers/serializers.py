from flask_restplus import fields

from cliniapi.endpoints.endpoints import api

paciente_json = api.model('Paciente', {
    'name': fields.String(required=True, description='nome do paciente'),
    'email': fields.String(required=True, description='email do paciente'),
})

procedimento_json = api.model('Procedimento', {
    'description': fields.String(required=True, description='descrição do procedimento'),
})

agendamento_json = api.model('Agendamento', {
    'day': fields.Date(required=True, description='dia do agendamento'),
    'start_hour': fields.DateTime(required=True, description='hoda de inicio do agendamento'),
    'end_hour': fields.DateTime(required=True, description='hoda de fim do agendamento'),
    'procedimento_id': fields.Integer(required=True, description='ID do procedimento'),
    'paciente_id': fields.Integer(required=True, description='ID do paciente'),

})
