import unittest
import requests

baseurl = 'http://127.0.0.1:5000/api/procedimentos/'


class ProcedimentoTestCase(unittest.TestCase):
    def test_get_procedimentos(self):
        rv = requests.get(baseurl)
        assert rv

    def test_get_procedimento(self):
        rv = requests.get(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_post_procedimento(self):
        param = {
            "description": "string"
        }
        rv = requests.post(baseurl, data=param)
        assert rv

    def test_delete_procedimento(self):
        rv = requests.delete(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_put_procedimento(self):
        param = {
            "description": "string"
        }
        rv = requests.put(baseurl + '1', data=param)
        assert rv.status_code in [400, 204]


if __name__ == '__main__':
    unittest.main()
