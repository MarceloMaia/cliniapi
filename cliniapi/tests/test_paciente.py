import unittest
import requests

baseurl = 'http://127.0.0.1:5000/api/pacientes/'


class PacienteTestCase(unittest.TestCase):
    def test_get_pacientes(self):
        rv = requests.get(baseurl)
        assert rv

    def test_get_paciente(self):
        rv = requests.get(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_post_paciente(self):
        param = {
            "name": "nome",
            "email": "marcelo@marcelo.com"
        }
        rv = requests.post(baseurl, data=param)
        assert rv

    def test_delete_paciente(self):
        rv = requests.delete(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_put_paciente(self):
        param = {
            "name": "nome 2",
            "email": "xxx@xxx.com"
        }
        rv = requests.put(baseurl + '1', data=param)
        assert rv.status_code in [400, 204]


if __name__ == '__main__':
    unittest.main()
