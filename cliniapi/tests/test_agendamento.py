import unittest
import requests

baseurl = 'http://127.0.0.1:5000/api/agendamentos/'


class AgendamentoTestCase(unittest.TestCase):
    def test_get_agendamentos(self):
        rv = requests.get(baseurl)
        assert rv

    def test_get_agendamento(self):
        rv = requests.get(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_post_agendamento(self):
        param = {
            "day": "2017-12-10",
            "start_hour": "2017-12-10T21:08:30.362Z",
            "end_hour": "2017-12-10T21:08:30.362Z",
            "procedimento_id": 1,
            "paciente_id": 1
        }
        rv = requests.post(baseurl, data=param)
        print(rv)
        assert rv

    def test_delete_agendamento(self):
        rv = requests.delete(baseurl + '1')
        assert rv.status_code in [400, 204]

    def test_put_agendamento(self):
        param = {
            "day": "2017-12-10",
            "start_hour": "2017-12-10T21:08:30.362Z",
            "end_hour": "2017-12-10T21:08:30.362Z",
            "procedimento_id": 1,
            "paciente_id": 1
        }
        rv = requests.put(baseurl + '1', data=param)
        assert rv.status_code in [400, 204]


if __name__ == '__main__':
    unittest.main()
