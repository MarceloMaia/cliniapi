import datetime

from cliniapi.database import init_db, db_session

from cliniapi.models.models import Paciente, Procedimento, Agendamento

init_db()


def format_date(date):
    return date.strftime('%Y-%m-%d')


def format_datetime(date):
    return date.isoformat()


def to_date(date_str):
    return datetime.datetime.strptime(date_str, '%Y-%m-%d')


def to_hour(date_str):
    '2017-12-10T16:21:32.644Z'
    # FIXME: what .644Z is about?
    return datetime.datetime.strptime(date_str[:-5], '%Y-%m-%dT%H:%M:%S')


#
# Paciente
#

def create_paciente(param):
    paciente = Paciente(name=param.get('name'), email=param.get('email'))
    db_session.add(paciente)
    db_session.commit()
    return {'id': paciente.id,
            'name': paciente.name,
            'email': paciente.email}, 201


def get_pacientes():
    pacientes = Paciente.query.all()
    lista = [{'id': p.id, 'name': p.name, 'email': p.email} for p in pacientes]
    if not lista:
        return lista, 204
    return lista, 200


def delete_paciente(paciente_id):
    paciente = Paciente.query.get(paciente_id)
    if not paciente:
        return {'error': 'paciente inexistente'}, 400
    agendamento = Agendamento.query.filter_by(paciente_id=paciente_id).first()
    if agendamento:
        return {'error': 'Apague o(s) agendamentos vinculados primeiro'}, 500
    db_session.delete(paciente)
    db_session.commit()
    return {'message': 'paciente #{} apagado'.format(paciente_id),
            'id': paciente_id}, 204


def get_paciente(paciente_id):
    paciente = Paciente.query.get(paciente_id)
    if not paciente:
        return {'error': 'paciente inexistente'}, 400
    return {'id': paciente_id,
            'name': paciente.email,
            'email': paciente.email}, 200


def update_paciente(paciente_id, data):
    paciente = Paciente.query.get(paciente_id)
    if not paciente:
        return {'error': 'paciente inexistente'}, 400
    paciente.name = data.get('name')
    paciente.email = data.get('email')
    db_session.commit()
    return {'id': paciente_id,
            'name': paciente.email,
            'email': paciente.email}, 204


#
# Procedimentos
#

def create_procedimento(param):
    procedimento = Procedimento(description=param.get('description'))
    db_session.add(procedimento)
    db_session.commit()
    return {'id': procedimento.id,
            'description': procedimento.description}, 201


def get_procedimentos():
    procedimentos = Procedimento.query.all()
    lista = [{'id': p.id, 'description': p.description} for p in procedimentos]
    if not lista:
        return lista, 204
    return lista, 200


def delete_procedimento(procedimento_id):
    procedimento = Procedimento.query.get(procedimento_id)
    if not procedimento:
        return {'error': 'procedimento inexistente'}, 400
    agendamento = Agendamento.query.filter_by(procedimento_id=procedimento_id).first()
    if agendamento:
        return {'error': 'Apague o(s) agendamentos vinculados primeiro'}, 500
    db_session.delete(procedimento)
    db_session.commit()
    return {'message': 'procedimento #{} apagado'.format(procedimento_id),
            'id': procedimento_id}, 204


def get_procedimento(procedimento_id):
    procedimento = Procedimento.query.get(procedimento_id)
    if not procedimento:
        return {'error': 'procedimento inexistente'}, 400
    return {'id': procedimento_id,
            'description': procedimento.description}, 200


def update_procedimento(procedimento_id, data):
    procedimento = Procedimento.query.get(procedimento_id)
    if not procedimento:
        return {'error': 'procedimento inexistente'}, 400
    procedimento.description = data.get('description')
    db_session.commit()
    return {'id': procedimento_id,
            'description': procedimento.description}, 204


#
# Agendamentos
#

def get_agendamentos():
    agendamentos = Agendamento.query.all()
    lista = [{'id': p.id,
              'day': format_date(p.day),
              'start_hour': format_datetime(p.start_hour),
              'end_hour': format_datetime(p.end_hour),
              'procedimento_id': p.procedimento_id,
              'paciente_id': p.paciente_id} for p in agendamentos]
    if not lista:
        return lista, 204
    return lista, 200


def create_agendamento(param):
    proc_id = param.get('procedimento_id')
    pac_id = param.get('paciente_id')
    proc = Procedimento.query.get(proc_id)
    pac = Paciente.query.get(pac_id)
    if not proc or not pac:
        return {'error': 'procedimento ou paciente inexistente'}, 400
    agendamento = Agendamento(day=to_date(param.get('day')),
                              start_hour=to_hour(param.get('start_hour')),
                              end_hour=to_hour(param.get('end_hour')),
                              procedimento=proc,
                              paciente=pac)
    db_session.add(agendamento)
    db_session.commit()
    return {'id': agendamento.id,
            'day': format_date(agendamento.day),
            'start_hour': format_datetime(agendamento.start_hour),
            'end_hour': format_datetime(agendamento.end_hour),
            'procedimento_id': agendamento.procedimento_id,
            'paciente_id': agendamento.paciente_id}, 201


def delete_agendamento(agendamento_id):
    agendamento = Agendamento.query.get(agendamento_id)
    if not agendamento:
        return {'error': 'agendamento inexistente'}, 400
    db_session.delete(agendamento)
    db_session.commit()
    return {'message': 'agendamento #{} apagado'.format(agendamento_id),
            'id': agendamento_id}, 204


def get_agendamento(agendamento_id):
    agendamento = Agendamento.query.get(agendamento_id)
    if not agendamento:
        return {'error': 'agendamento inexistente'}, 400
    return {'id': agendamento.id,
            'day': format_date(agendamento.day),
            'start_hour': format_datetime(agendamento.start_hour),
            'end_hour': format_datetime(agendamento.end_hour),
            'procedimento_id': agendamento.procedimento_id,
            'paciente_id': agendamento.paciente_id}, 200


def update_agendamento(agendamento_id, param):
    agendamento = Agendamento.query.get(agendamento_id)
    if not agendamento:
        return {'error': 'agendamento inexistente'}, 400

    proc_id = param.get('procedimento_id')
    pac_id = param.get('paciente_id')
    proc = Procedimento.query.get(proc_id)
    pac = Paciente.query.get(pac_id)
    if not proc or not pac:
        return {'error': 'procedimento ou paciente inexistente'}, 400

    agendamento.day = to_date(param.get('day'))
    agendamento.start_hour = to_hour(param.get('start_hour'))
    agendamento.end_hour = to_hour(param.get('end_hour'))
    agendamento.procedimento = proc
    agendamento.paciente = pac
    db_session.commit()
    return {'id': agendamento.id,
            'day': format_date(agendamento.day),
            'start_hour': format_datetime(agendamento.start_hour),
            'end_hour': format_datetime(agendamento.end_hour),
            'procedimento_id': agendamento.procedimento_id,
            'paciente_id': agendamento.paciente_id}, 204
