from cliniapi.database import init_db
from cliniapi.endpoints.endpoints import app
# TODO criar um sistema de logs

if __name__ == '__main__':
    init_db()                          # inicia o db
    app.run(debug=True)                # inicia o server
