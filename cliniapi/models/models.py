import datetime

from sqlalchemy import Column, Integer, String, Date, Time
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.schema import ForeignKey

from cliniapi.database import Base


class Paciente(Base):
    __tablename__ = 'paciente'
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    email = Column(String(120))

    def __init__(self, name=None, email=None):
        self.name = name
        self.email = email

    def __repr__(self):
        return '<Paciente: {}>'.format(self.name)


class Procedimento(Base):
    __tablename__ = 'procedimento'
    id = Column(Integer, primary_key=True)
    description = Column(String(50))

    def __init__(self, description=None):
        self.description = description

    def __repr__(self):
        return '<Procedimento: {}'.format(self.description)


class Agendamento(Base):
    __tablename__ = 'agendamento'
    id = Column(Integer, primary_key=True)
    day = Column(Date, default=datetime.datetime.now())
    start_hour = Column(Time, default=datetime.datetime.now())
    end_hour = Column(Time, default=datetime.datetime.now() + datetime.timedelta(100))

    procedimento_id = Column(Integer, ForeignKey("procedimento.id"), nullable=False)
    procedimento = relationship('Procedimento',
                                backref=backref('agendamento', lazy=True))

    paciente_id = Column(Integer, ForeignKey("paciente.id"), nullable=False)
    paciente = relationship('Paciente',
                            backref=backref('agendamento', lazy=True))

    def __init__(self, day, start_hour, end_hour, procedimento, paciente):
        self.day = day
        self.start_hour = start_hour
        self.end_hour = end_hour
        self.procedimento = procedimento
        self.paciente = paciente

    def __repr__(self):
        return '<Agendamento: #{} cliente {}'.format(self.id, self.paciente)
