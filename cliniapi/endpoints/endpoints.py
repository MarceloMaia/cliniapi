from flask import Flask
from flask import request
from flask_restplus import Api
from flask_restplus import Resource

from cliniapi.utils import create_paciente, get_pacientes, get_procedimentos, create_procedimento, get_agendamentos, \
    create_agendamento, delete_agendamento, delete_procedimento, delete_paciente, get_paciente, update_paciente, \
    get_procedimento, update_procedimento, update_agendamento, get_agendamento

app = Flask(__name__)          # Create a Flask WSGI appliction
api = Api(app, prefix='/api')  # Create a Flask-RESTPlus API
from cliniapi.serializers.serializers import paciente_json, agendamento_json, procedimento_json


#
# namespaces
#

ns_pacientes = api.namespace('pacientes', description='Operações relacionadas a pacientes')
ns_procedimentos = api.namespace('procedimentos', description='Operações relacionadas a procedimentos')
ns_agendamentos = api.namespace('agendamentos', description='Operações relacionadas a agendamentos')

api.add_namespace(ns_pacientes)
api.add_namespace(ns_procedimentos)
api.add_namespace(ns_agendamentos)


#
# Pacientes
#

@ns_pacientes.route('/')
class PacientesCollection(Resource):
    def get(self):
        """Retorna uma lista de pacientes."""
        return get_pacientes()

    @api.response(201, 'Paciente criado com sucesso.')
    @api.expect(paciente_json)
    def post(self):
        """Cria um novo paciente."""
        return create_paciente(request.json)


@ns_pacientes.route('/<int:id>')
@api.response(404, 'Paciente não encontrado.')
class PacientesItem(Resource):
    def get(self, id):
        """Retorna detalhes sobre um paciente."""
        return get_paciente(id)

    @api.response(204, 'Paciente atualizado com sucesso.')
    @api.expect(paciente_json)
    def put(self, id):
        """Atualiza um paciente."""
        return update_paciente(id, request.json)

    @api.response(204, 'Paciente deletado com sucesso.')
    def delete(self, id):
        """Deleta um paciente."""
        return delete_paciente(id)


#
# Procedimentos
#

@ns_procedimentos.route('/')
class ProcedimentosCollection(Resource):
    def get(self):
        """Retorna uma lista de procedimentos."""
        return get_procedimentos()

    @api.response(201, 'Procedimento criado com sucesso.')
    @api.expect(procedimento_json)
    def post(self):
        """Cria um novo procedimento."""
        return create_procedimento(request.json)


@ns_procedimentos.route('/<int:id>')
@api.response(404, 'Procedimento não encontrado.')
class ProcedimentosItem(Resource):
    def get(self, id):
        """Retorna detalhes sobre um procedimento."""
        return get_procedimento(id)

    @api.response(204, 'Procedimento atualizado com sucesso.')
    @api.expect(procedimento_json)
    def put(self, id):
        """Atualiza um procedimento."""
        return update_procedimento(id, request.json)

    @api.response(204, 'Procedimento deletado com sucesso.')
    def delete(self, id):
        """Deleta um procedimento."""
        return delete_procedimento(id)


#
# Agendamentos
#

@ns_agendamentos.route('/')
class AgendamentosCollection(Resource):
    def get(self):
        """Retorna uma lista de agendamentos."""
        return get_agendamentos()

    @api.response(201, 'Agendamento criado com sucesso.')
    @api.expect(agendamento_json)
    def post(self):
        """Cria um novo agendamento."""
        return create_agendamento(request.json)


@ns_agendamentos.route('/<int:id>')
@api.response(404, 'Agendamento não encontrado.')
class AgendamentosItem(Resource):
    def get(self, id):
        """Retorna detalhes de uma agendamento."""
        return get_agendamento(id)

    @api.response(204, 'Agendamento atualizado com sucesso.')
    @api.expect(agendamento_json)
    def put(self, id):
        """Atualiza um agendamento."""
        return update_agendamento(id, request.json)

    @api.response(204, 'Agendamento deletado com sucesso.')
    def delete(self, id):
        """Deleta um agendamento."""
        return delete_agendamento(id)
